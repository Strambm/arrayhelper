<?php

namespace Cetria\Helpers\ArrayHelper;

class ArrayHelper
{
    /**
     * @param array $array
     * @param string $keyOrIndex
     * @param bool $strict
     * @return array
     */
    public static final function unsetKeyIfExist(array $array, string $keyOrIndex): array
    {
        if(array_key_exists($keyOrIndex, $array)) {
            unset($array[$keyOrIndex]);
        }
        return $array;
    }
}
