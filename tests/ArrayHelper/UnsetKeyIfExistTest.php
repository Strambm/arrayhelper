<?php

namespace Cetria\Helpers\ArrayHelper\Tests\ArrayHelper;

use Cetria\Helpers\ArrayHelper\ArrayHelper;
use PHPUnit\Framework\TestCase;

class UnsetKeyIfExistTest extends TestCase
{
    public function testMethod(): void
    {
        $key = 'key';
        $array = [$key => 'value'];
        $returnedArray = ArrayHelper::unsetKeyIfExist($array, $key);
        $this->assertEquals(0, count($returnedArray));
    }

    public function testMethod__keyunexist(): void
    {
        $array[]  = 'test';
        $returnedArray = ArrayHelper::unsetKeyIfExist($array, 'key');
        $this->assertEquals(1, count($returnedArray));
    }

    public function testMethod__stringIndex__intKey(): void
    {
        $array['0']  = 'test';
        $returnedArray = ArrayHelper::unsetKeyIfExist($array, 0);
        $this->assertEquals(0, count($returnedArray));
    }

    public function testMethod__stringIndext__stringKey(): void
    {
        $array['0']  = 'test';
        $returnedArray = ArrayHelper::unsetKeyIfExist($array, '0');
        $this->assertEquals(0, count($returnedArray));
    }

    public function testMethod__intIndex__intKey(): void
    {
        $array[]  = 'test';
        $returnedArray = ArrayHelper::unsetKeyIfExist($array, 0);
        $this->assertEquals(0, count($returnedArray));
    }

    public function testMethod__intIndex__stringKey(): void
    {
        $array[]  = 'test';
        $returnedArray = ArrayHelper::unsetKeyIfExist($array, '0');
        $this->assertEquals(0, count($returnedArray));
    }

    public function testMethod__badKey(): void
    {
        $array[]  = 'test';
        $returnedArray = ArrayHelper::unsetKeyIfExist($array, 'idk');
        $this->assertEquals($array, $returnedArray);
    }
}